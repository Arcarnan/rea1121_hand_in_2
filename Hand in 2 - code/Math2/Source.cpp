#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>       // High resolution clock

using namespace std;

const int windowX = 512;
const int windowY = 512;
const float dotRad = 16.0;
const int numDots = 512 / 32;  // 16
const int MAXCORDS = 9;

const sf::Color dotColour[8] = 
{ sf::Color::Red, sf::Color::White, sf::Color::Blue,
sf::Color::Green, sf::Color::Cyan, sf::Color::Magenta,
sf::Color::Yellow, sf::Color::Black };

void readFromFile();

class Dot
{
public:
	virtual void draw(sf::RenderWindow& w);

	sf::Vector2f pos;
	int type;
	bool toTerminate = false;
};

void Dot::draw(sf::RenderWindow& w)
{
	sf::CircleShape s;
	s.setRadius(dotRad);
	s.setOrigin(dotRad, dotRad);
	s.setFillColor(dotColour[type]);
	s.setPosition(pos);
	w.draw(s);

	if (toTerminate)		//visualize those marked to be deleted
	{
		s.setRadius(dotRad / 2.0f);
		s.setFillColor(sf::Color::Black);
		s.setOrigin(dotRad * 0.5f, dotRad * 0.5f);
		w.draw(s);
	}
}

struct Pattern
{
	int numberOfPatterns = 0; 
	int cord[MAXCORDS][2];	//coordinates
	int size = 0;		//number of coordinates in the pattern
};

Pattern pat[MAXCORDS];

class Board
{
public:
	Dot dots[numDots + 4][numDots + 4];

	Board();
	virtual void update();
	virtual void draw(sf::RenderWindow& w);
	int pattern[numDots];
};

void Board::update()
{
	int color;
	int numberEqual;
	int x;
	int y;
					//coordinates: dots[y][x]

	for (int i = 0; i < numDots; i++)		//for all rows
	{
		for (int j = 0; j < numDots; j++)		//for all columns
		{
	
			if (dots[i + 2][j + 2].toTerminate == true)		
			{				//if marked for termination
				dots[i + 2][j + 2].toTerminate = false;

				for (int k = i; k >= 0; --k)	
				{
					dots[k + 2][j + 2].type = dots[k + 1][j + 2].type;		
												//move down the color above
					if (k == 0)		//if moved from the tom
					{					//insert new random
						dots[k + 2][j + 2].type = rand() % 7;	
					}
				}
			}
		}
	}

	for (int i = 0; i < numDots; i++)		//search all rows
	{
		for (int j = 0; j < numDots; j++)	//search all columns
		{
			for (int k = 0; k < pat[0].numberOfPatterns; k++)
			{							//go through all paterns
				numberEqual = 0;
				color = dots[i + 2][j + 2].type;	
								//color of the 0,0 coordinate

				bool patternMatch = true;
				for (int l = 0; l < pat[k].size; l++)	
				{				//check for all coordinates
					x = pat[k].cord[l][0] + j + 2;
					y = pat[k].cord[l][1] + i + 2;

					if (dots[y][x].type != color)	//if not the same color
					{
						patternMatch = false;
					}
				}

				if (patternMatch)
				{
					for (int l = 0; l < pat[k].size; l++)	
					{						//check for all  coordinates
						x = pat[k].cord[l][0] + j + 2;
						y = pat[k].cord[l][1] + i + 2;

						dots[y][x].toTerminate = true;
					}
					dots[i + 2][j + 2].toTerminate = true;
				}
			}
		}
	}
}

Board::Board()
{
	// Initialize interior
	for (int i = 0; i < numDots; i++)
	{
		for (int j = 0; j < numDots; j++)
		{
			dots[i + 2][j + 2].type = rand() % 7;  
						// Black is never used in interior, hence % 7 not % 8
			dots[i + 2][j + 2].pos = sf::Vector2f
			(j * 2 * dotRad + dotRad, i * 2 * dotRad + dotRad);

		}
	}

	// Inititialize boundary
	for (int i = 0; i < numDots; i++)
	{
		dots[i][0].type = 7;
		dots[i][1].type = 7;
		dots[0][i].type = 7;
		dots[1][i].type = 7;
		dots[18][i].type = 7;
		dots[19][i].type = 7;
		dots[i][18].type = 7;
		dots[i][19].type = 7;
	}
}

void Board::draw(sf::RenderWindow& w)
{
	for (int i = 0; i < numDots; i++)
	{
		for (int j = 0; j < numDots; j++)
		{
			dots[i + 2][j + 2].draw(w);
		}
	}
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(windowX, windowY), "My window");
	int x;
	int y;
	bool pressed = false;

	readFromFile();

	Board board;

	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{

			if (event.type == sf::Event::Closed)
			{
				window.close();
				exit(0);
			}

			if (!pressed)
			{
				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						x = event.mouseButton.x / (2 * dotRad);
						y = event.mouseButton.y / (2 * dotRad);
						pressed = true;
					}
				}
			}
			else
			{
				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						int ax = event.mouseButton.x / (2 * dotRad);
						int ay = event.mouseButton.y / (2 * dotRad);
						int type = board.dots[y + 2][x + 2].type;
						board.dots[y + 2][x + 2].type = 
							board.dots[ay + 2][ax + 2].type;
						board.dots[ay + 2][ax + 2].type = type;
						pressed = false;
					}
				}
			}
		}
		board.update();
		window.clear(sf::Color::Black);
		board.draw(window);
		window.display();
	}
	return 0;
}

void readFromFile()
{
	int num;		//number of coordinates
	int numPat;		//number of patterns
	int x;
	int y;

	ifstream infile;
	infile.open("PATTERN.DTA");	//opens the "PATTERN.DTA" file

	if (infile)
	{
		cout << "\nReading patterns from 'PATTERN.DTA'\n";
		//<number of coordinates> - <x,y> - <x,y>
		infile >> numPat;
		infile.ignore();
		pat[0].numberOfPatterns = numPat;

		for (int j = 0; j < numPat; j++)	//goes through all the patterns
		{
			infile >> num;
			pat[j].size = num;

			for (int i = 0; i < num; i++)	
			{		//goes through all the coordinates for the pattern
				infile >> x;
				infile >> y;

				pat[j].cord[i][0] = x;
				pat[j].cord[i][1] = y;
			}
			infile.ignore();
		}
		infile.close();
	}
	else	//if file not found
		cout << "\nCannot find file 'PATTERN.DTA'\n";
}